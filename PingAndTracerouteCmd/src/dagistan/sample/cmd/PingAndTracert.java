package dagistan.sample.cmd;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class PingAndTracert {

	public static void main(String[] args) {

		PingAndTracert obj = new PingAndTracert();

		String hostOrIp = "dagistankaradeniz.com";

		String output = obj.executePingCommand(hostOrIp);
		System.out.println(output);

		String output2 = obj.executeTraceRouteCommand(hostOrIp);
		System.out.println(output2);
	}

	private String executePingCommand(String hostOrIp) {

		StringBuffer output = new StringBuffer();
		String command = "ping -n 3 " + hostOrIp; // for windows

		Process p;

		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String line = "";
			while ((line = reader.readLine()) != null) {
				output.append(line + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output.toString();
	}

	private String executeTraceRouteCommand(String hostOrIp) {

		StringBuffer output = new StringBuffer();
		String command = "tracert " + hostOrIp; // for windows

		Process p;

		System.out.println("Please wait for Tracert command results. This process can take a while.");
		
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String line = "";
			while ((line = reader.readLine()) != null) {
				output.append(line + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output.toString();
	}

}
