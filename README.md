# README #

Run as Java Application.

### Used Technologies for all projects ###

* Java SE,
* Java EE,
* 3rd party Java frameworks / libraries.

### Requirements ###

* Eclipse IDE
* JDK 1.8
* Deployment and Run instructions are included in related project class files. Read carefully.

I hope you enjoy :)